/**
 * File: /next.config.js
 * Project: @multiplatform/next
 * File Created: 23-01-2022 02:18:40
 * Author: Clay Risser
 * -----
 * Last Modified: 19-11-2022 14:14:17
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const path = require('path');
const transpileModules = require('app/transpileModules');
const withBabelMinify = require('next-babel-minify');
const withBundleAnalyzer = require('@next/bundle-analyzer');
const withFonts = require('next-fonts');
const withImages = require('next-images');
const withPlugins = require('next-compose-plugins');
const withTranspileModules = require('next-transpile-modules');
const { PHASE_DEVELOPMENT_SERVER } = require('next/constants');
const { withExpo } = require('@expo/next-adapter');
const pkg = require('./package.json');
const privateConfig = require('../../app/config/private');
const publicConfig = require('../../app/config/public');
const sharedConfig = require('../../app/config/config');

module.exports = (phase, config, ...args) => {
  /**
   * @type {import('next').NextConfig}
   */
  let nextConfig = {
    assetPrefix: '',
    poweredByHeader: false,
    output: 'standalone',
    reactStrictMode: false,
    devIndicators: {
      buildActivity: true,
      buildActivityPosition: 'bottom-right',
    },
    experimental: {
      outputFileTracingRoot: path.join(__dirname, '../../'),
      externalDir: true,
    },
    i18n: undefined,
    amp: undefined,
    webpack: (config) => {
      if (!config.watchOptions) config.watchOptions = {};
      config.watchOptions.ignored = '**/.*';
      return config;
    },
    publicRuntimeConfig: {
      ...publicConfig,
    },
    serverRuntimeConfig: {
      ...privateConfig,
    },
  };
  const plugins = [
    withTranspileModules([...new Set([...transpileModules, ...pkg.transpileModules])], {
      resolveSymlinks: true,
      debug: false,
    }),
    [withExpo, { projectRoot: __dirname }],
    withFonts,
    withImages,
    withBabelMinify,
  ];
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    plugins.concat([
      withBundleAnalyzer({
        enabled: sharedConfig.DEBUG === '1',
      }),
    ]);
  }
  nextConfig = withPlugins(plugins, nextConfig)(phase, config, ...args);
  delete nextConfig.configOrigin;
  delete nextConfig.projectRoot;
  delete nextConfig.target;
  delete nextConfig.webpack5;
  delete nextConfig.webpackDevMiddleware;
  if (nextConfig.assetPrefix === '') delete nextConfig.assetPrefix;
  return nextConfig;
};
