/**
 * File: /pages/_app.tsx
 * Project: @multiplatform/next
 * File Created: 23-01-2022 02:18:40
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 16:13:32
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// INFO: fixes https://github.com/software-mansion/react-native-reanimated/issues/3355
if (typeof window !== 'undefined') window._frameTimestamp = null;
import 'raf/polyfill';
import Head from 'next/head';
import React, { FC } from 'react';
import type { AppProps } from 'next/app';
import { GlobalProvider } from 'app/provider';

const App: FC<AppProps> = ({ Component, pageProps }: AppProps) => (
  <GlobalProvider>
    <Head>
      <title>Multiplatform Framework</title>
      <meta name="description" content="write once, render anywhere" />
      <link rel="icon" href="/favicon.icon" />
    </Head>
    <Component {...pageProps} />
  </GlobalProvider>
);

export default App;

declare global {
  interface Window {
    _frameTimestamp: unknown;
  }
}
