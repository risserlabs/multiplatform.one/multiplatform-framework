/**
 * File: /screens/Home/Home.stories.tsx
 * Project: app
 * File Created: 19-08-2022 08:03:24
 * Author: Clay Risser
 * -----
 * Last Modified: 26-09-2022 04:58:35
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Text } from 'react-native';
import { HomeScreen } from './index';

export default {
  title: 'screens/Home',
  component: HomeScreen,
  parameters: {
    status: { type: 'beta' },
  },
};

export const test = () => <Text>I am a story</Text>;

// // export const main = () => <HomeScreen />;
