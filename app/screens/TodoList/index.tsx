/**
 * File: /screens/TodoList/index.tsx
 * Project: app
 * File Created: 23-09-2022 07:33:59
 * Author: Ajith Kumar
 * -----
 * Last Modified: 02-12-2022 17:59:52
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Card } from '@native-theme-ui/core';
import { TodoBody } from 'app/organisms/TodoBody';
import { createParam } from '@risserlabs/solito/build';
import { Header } from 'app/components/molecules/Header';

const { useParam } = createParam();

export const TodoListScreen = () => {
  const [todoListName] = useParam('todoListName');
  console.log('todoListName', todoListName);
  return (
    <Card
      style={{
        backgroundColor: '#1F1F1E',
        alignItems: 'center',
        padding: 20,
        margin: 10,
        borderRadius: 20,
      }}
    >
      <Header description="Created with React,Recoil and TypeScript">Todo List {todoListName}</Header>
      <TodoBody />
    </Card>
  );
};
