/**
 * File: /theme/util.ts
 * Project: app
 * File Created: 11-11-2022 04:47:27
 * Author: Clay Risser
 * -----
 * Last Modified: 12-11-2022 12:22:09
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DripsyBaseTheme, makeTheme } from 'dripsy';
import { ExtendedCustomTheme as ThemeUiExtendedCustomTheme, variants as themeUiVariants } from '@native-theme-ui/core';
import { ExtendedCustomTheme, variants } from './variants';
import { colors } from './colors';

export function wrapTheme(theme: Record<string, any>) {
  return makeTheme({
    ...theme,
    ...themeUiVariants,
    ...variants,
    colors: {
      ...colors,
      ...theme.colors,
    },
    autoContrast: 'AAA',
  } as DripsyBaseTheme);
}

declare module 'dripsy' {
  interface DripsyCustomTheme extends ThemeUiExtendedCustomTheme, ExtendedCustomTheme {}
}
