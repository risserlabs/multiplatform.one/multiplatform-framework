/**
 * File: /theme/variants.ts
 * Project: app
 * File Created: 07-11-2022 04:01:50
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 14:59:35
 * Modified By: K S R P BHUSHAN
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const variants: Partial<ExtendedCustomTheme> = {
  cardButtons: {
    giftBox: {
      backgroundColor: 'secondary',
    },
  },
  giftCards: {
    giftCard: {
      backgroundColor: 'giftCard.background.1',
      color: 'giftCard.text.1',
    },
    giftCard2: {
      backgroundColor: 'giftCard.background.2',
    },
    giftCard3: {
      backgroundColor: 'highlight',
    },
    premiumExtensionGiftCards: {
      color: '#FFFFFF',
    },
  },
  count: {
    countVariant: {
      backgroundColor: 'lightBlue',
    },
    countVariant2: {
      backgroundColor: 'background',
    },
  },
  selectContact: {
    contactVariant: {
      backgroundColor: 'highlight',
    },
  },
  walletMenuItem: {
    menuItem: {
      backgroundColor: 'walletMenuItem.backgroundColor.1',
    },
    menuItem2: {
      backgroundColor: 'walletMenuItem.backgroundColor.2',
    },
    menuItem3: {
      backgroundColor: 'walletMenuItem.backgroundColor.3',
    },
    menuItem4: {
      backgroundColor: 'walletMenuItem.backgroundColor.4',
    },
  },
  walletMenu: {
    menu: {
      backgroundColor: 'primary',
    },
    menu2: {
      backgroundColor: '#	FFD700',
    },
  },
  modal: {
    modalVariant: {
      backgroundColor: 'modal.headerColor.2',
      color: 'modal.text.1',
    },
    modalVariant2: {
      backgroundColor: 'modal.headerColor.1',
    },
  },
  totalPriceBox: {
    variant1: {
      backgroundColor: 'totalPriceBox.backgroundColor.1',
    },
    variant2: {
      backgroundColor: 'totalPriceBox.backgroundColor.1',
      color: 'blue',
    },
    variant3: {
      backgroundColor: 'totalPriceBox.backgroundColor.2',
      color: 'orange',
    },
  },
  textColor: {
    textVariant: { color: 'textLightBlue' },
  },
  transactionLog: {
    variant1: { color: 'transactionLog.text.1' },
    variant2: { color: 'transactionLog.text.2' },
  },
};

export interface ExtendedCustomTheme {
  cardButtons: {
    giftBox: Record<string, unknown>;
  };
  giftCards: {
    giftCard: Record<string, unknown>;
    giftCard2: Record<string, unknown>;
    giftCard3: Record<string, unknown>;
    premiumExtensionGiftCards: Record<string, unknown>;
  };
  count: {
    countVariant: Record<string, unknown>;
    countVariant2: Record<string, unknown>;
  };
  selectContact: {
    contactVariant: Record<string, unknown>;
  };
  walletMenuItem: {
    menuItem: Record<string, unknown>;
    menuItem2: Record<string, unknown>;
    menuItem3: Record<string, unknown>;
    menuItem4: Record<string, unknown>;
  };
  walletMenu: {
    menu: Record<string, unknown>;
    menu2: Record<string, unknown>;
  };
  modal: {
    modalVariant: Record<string, unknown>;
    modalVariant2: Record<string, unknown>;
  };
  totalPriceBox: {
    variant1: Record<string, unknown>;
    variant2: Record<string, unknown>;
    variant3: Record<string, unknown>;
  };
  textColor: {
    textVariant: Record<string, unknown>;
  };
  transactionLog: {
    variant1: Record<string, unknown>;
    variant2: Record<string, unknown>;
  };
}
