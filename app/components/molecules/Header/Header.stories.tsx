/**
 * File: /components/molecules/Header/Header.stories.tsx
 * Project: app
 * File Created: 20-09-2022 01:30:30
 * Author: Ajith Kumar
 * -----
 * Last Modified: 03-10-2022 07:25:55
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { Header } from '.';

export default {
  title: 'components/molecules/Header',
  component: Header,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <Header description="Created with someThing">I am header component</Header>;
