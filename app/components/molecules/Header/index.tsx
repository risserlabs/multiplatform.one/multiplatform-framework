/**
 * File: /components/molecules/Header/index.tsx
 * Project: app
 * File Created: 20-09-2022 00:48:09
 * Author: Ajith Kumar
 * -----
 * Last Modified: 07-10-2022 00:24:56
 * Modified By: Lavanya
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ReactNode } from 'react';
import { Box } from 'app/components/elements/Box';
import { Text } from '../../elements/Text';

interface HeaderProps {
  children: ReactNode;
  description: string;
}

export const Header: FC<HeaderProps> = (props: HeaderProps) => {
  return (
    <Box style={{ padding: 10, marginTop: 10 }}>
      <Text
        style={{
          textAlign: 'center',
          color: 'gray',
          fontSize: 30,
          fontWeight: 'bold',
        }}
      >
        {props.children}
      </Text>
      <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: '600' }}>{props.description}</Text>
    </Box>
  );
};

Header.defaultProps = {
  description: '',
};
