/**
 * File: /components/elements/Button/index.tsx
 * Project: app
 * File Created: 20-09-2022 07:32:41
 * Author: Ajith Kumar
 * -----
 * Last Modified: 20-09-2022 07:42:36
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Button as NButton } from '@native-theme-ui/core';
import { styled } from 'dripsy';
import { ComponentProps } from 'react';

export const Button = styled(NButton)({});

export type ButtonProps = ComponentProps<typeof Button>;
