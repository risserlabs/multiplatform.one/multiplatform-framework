/**
 * File: /organisms/TodoBody/TodoBody.stories.tsx
 * Project: app
 * File Created: 22-09-2022 07:14:54
 * Author: Ajith Kumar
 * -----
 * Last Modified: 22-09-2022 07:21:41
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { TodoBody } from '.';

export default {
  title: 'organisms/TodoBody',
  component: TodoBody,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <TodoBody />;
