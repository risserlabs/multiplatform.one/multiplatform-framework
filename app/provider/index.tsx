/**
 * File: /provider/index.tsx
 * Project: app
 * File Created: 27-11-2022 03:49:01
 * Author: Clay Risser
 * -----
 * Last Modified: 28-11-2022 09:49:35
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ReactNode, StrictMode } from 'react';
import { KeycloakConfig, AuthProvider, AuthConfig } from '@multiplatform.one/keycloak';
import { KeycloakInitOptions } from 'keycloak-js';
import { NavigationProvider } from './navigation/index';
import { StateProvider } from './state';
import { ThemeProvider } from './theme';
import { config } from 'app/config';
import { main } from 'app/theme/themes';

export interface ProviderProps {
  children: ReactNode;
}

export interface GlobalProviderProps extends ProviderProps {
  authConfig?: AuthConfig;
  cookies?: unknown;
  keycloak?: KeycloakConfig;
  keycloakInitOptions?: KeycloakInitOptions;
  noNavigation?: boolean;
  strict?: boolean;
  theme?: any;
}

export const GlobalProvider: FC<GlobalProviderProps> = (props: GlobalProviderProps) => {
  const render = () => (
    <ThemeProvider theme={props.theme}>
      {props.strict ? <StrictMode>{props.children}</StrictMode> : props.children}
    </ThemeProvider>
  );

  function renderNavigationProvider(children: ReactNode) {
    if (props.noNavigation) return <>{children}</>;
    return <NavigationProvider>{children}</NavigationProvider>;
  }

  function renderKeycloakProvider(children: ReactNode) {
    if (!props.keycloak) return <>{children}</>;
    return (
      <AuthProvider
        authConfig={props.authConfig}
        cookies={props.cookies}
        debug={config.get('DEBUG') === '1'}
        keycloakConfig={props.keycloak}
        keycloakInitOptions={props.keycloakInitOptions}
      >
        {children}
      </AuthProvider>
    );
  }

  function renderState(children: ReactNode) {
    return <StateProvider>{children}</StateProvider>;
  }

  return renderState(renderKeycloakProvider(renderNavigationProvider(render())));
};

GlobalProvider.defaultProps = {
  theme: main,
  noNavigation: false,
};
